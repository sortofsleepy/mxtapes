

void main (){
  vec3 aNormal = normal;
  vec3 aPosition = position;
  vec2 aUv = uv;

  vec4 p = vec4(position,1.);
  vec3 e = normalize(vec3(view * model  * p));
  vec3 n = normalize(normalMatrix * normal);
  vec3 r = reflect(e,n);
  float m = 2. * sqrt(pow(r.x,2.) + pow(r.y,2.) + pow(r.z + 1.,2.));
  vNormal =  r.xy / m + .5;

  gl_Position =  proj * view * model * vec4(position, 1.0);
}
