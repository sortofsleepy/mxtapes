vec3 saturate(vec3 a){return clamp(a,0.,1.);}
float rand(vec2 co){
    return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

vec3 background(float t, vec3 rd)
{
 vec3 light = vec3(0.);
 float sun = max(0.0, dot(rd, light));
 float sky = max(0.0, dot(rd, vec3(-sin(time), -cos(time), -1.0)));
 float ground = max(0.0, -dot(rd, vec3(-sin(time+PI*2.0), -cos(time + PI * 2.0), 1.0)));
 float bg = max(0.0, dot(rd, vec3(0.0,0.0, sin(time)*.2+1.0)));
 return
  (pow(sun, 256.0)+0.2*pow(sun, 4.0))*vec3(1.0, 1.6, 0.0) +
  pow(ground, 1.5)*vec3(sin(time)*.1+.4, 0.0, .3) +
  pow(sky, 1.5)*vec3(0.0, sin(time)*.1+.3, .4)+
  pow(bg, 2.0)*vec3(.5,.5,.5) ;
}

const vec2 iResolution = vec2(1024.0,678.0);
void main (){
    vec2 uv = gl_FragCoord.xy;
	uv = (uv / iResolution.y * 2.0) - 1.;
    uv.x += cos(uv.y* (uv.x+1.) * 3.) * 0.003;
    uv.y += cos(uv.x * 6.) * 0.00007;
    vec3 rd = normalize(vec3(uv, 1.0));
    vec3 base = texture2D(texture,vNormal).rgb;

    vec3 bgCol = vec3(1.);
    vec3 col = background(0.5, rd) * vec3(0.2, 0.8, 1.0);

    vec3 norm = mix(vec3(vNormal,1.0),col,0.7);

    // film grain-ish addition
    norm.rgb += (rand(uv)-.5)*0.8;
    norm.rgb += saturate(col.rgb);

    gl_FragColor = vec4( base * norm * 2.0 ,1.0);
}