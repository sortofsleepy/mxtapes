(ns macros.helpers)
(defmacro loadfile [filepath]
  (slurp filepath))

(defmacro forloop [[init test step] & body]
  `(loop [~@init]
     (when ~test
       ~@body
       (recur ~step))))