(ns mxtapes.capabilities
  (:require
   [mxtapes.globals :as globals]))


(def gl globals/gl)


(if (= (.getExtension gl "ANGLE_instanced_arrays") nil)
  (js/console.error "Your machine's graphics card does not appear to support instanced arrays"))



(if (= (.getExtension gl "OES_texture_float") nil)
  (js/console.error "Your machine's graphics card does not appear to support floating point texture arrays"))
