(ns mxtapes.blur-test
    (:require-macros
   [macros.helpers :refer [loadfile]])
    (:require
     [thi.ng.geom.gl.webgl.constants :as glc]
   [thi.ng.color.core :as col]
   [thi.ng.geom.vector :refer [vec2]]
   [mxtapes.globals :as globals]
   [thi.ng.geom.gl.core :as gl]
   [thi.ng.geom.gl.shaders :as sh]
   [thi.ng.geom.matrix :refer [M44]]
   [mxtapes.globals :as globals]
   [thi.ng.geom.gl.fx :as fx]
   [matchstick.core.fbo :as fbo]))


(def passthrough-vs
  "void main(){
  vUV=uv;
  gl_Position=  model * vec4(position,0.0,1.0);}")

(def passthrough-fs
  "void main(){
  gl_FragColor=texture2D(testtex,vUV);

  }")

(def shader-spec
  {:vs passthrough-vs
   :fs passthrough-fs
   :attribs  {:position  :vec2
              :uv        :vec2}
   :varying  {:vUV       :vec2}
   :uniforms {:model     [:mat4 M44]
              :proj      :mat4
              :view      :mat4
              :testtex [:sampler2D 1]
              :tex       [:sampler2D 0]}
   :state    {:depth-test false
              :blend true
              :blend-eq glc/func-add}})

;; runs a horizontal and vertical blur pass on a scene which can be written to
;; using the begin/end scene functions


(def blur-shader
   {:vs (loadfile "src/shadersource/blur.vert")
    :fs (loadfile "src/shadersource/blur.frag")
    :uniforms {:view :mat4
               :proj :mat4
               :model [:mat4 M44]
               :tex0 [:sampler2D 1]
               :sample_offset :vec2
               :attenuation :float}
         :attribs {:position :vec2
                   :uv :vec2}
         :varying {:vTexCoord0 :vec2}})


(def horizontalBlur (fx/init-fx-fbo globals/gl 512 512))
(def hBlurQuad (-> (fx/init-fx-quad globals/gl)
                (assoc :shader (sh/make-shader-from-spec globals/gl blur-shader))
                (assoc-in [:shader :state :tex] (:tex horizontalBlur))))



(def verticalBlur (fx/init-fx-fbo globals/gl 512 512))
(def vBlurQuad (-> (fx/init-fx-quad globals/gl)
                   (assoc :shader (sh/make-shader-from-spec globals/gl blur-shader))
                   (assoc-in [:shader :state :tex] (:tex verticalBlur))))



(def render (fx/init-render-fbo globals/gl js/window.innerWidth js/window.innerHeight))
(def renderQuad (-> (fx/init-fx-quad globals/gl)
                    (assoc :shader (sh/make-shader-from-spec globals/gl shader-spec))
                    (assoc-in [:shader :state :tex] (:tex render))))







;; Binds the scene fbo so that we can render the scene into it
(defn start-scene [gl]
  (let [{:keys [fbo width height]} render]
    (gl/bind fbo)    ;; set viewport
    (gl/set-viewport gl 0 0 width height)
    (gl/clear-color-and-depth-buffer globals/gl col/BLACK 1)
    
    ))


;; ends the rendering onto the scene fbo
(defn end-scene []
  (let [{:keys [fbo]} render]
    (gl/unbind fbo)))



(defn horizontal-blur [gl]
  "Runs the horizontal blur on the scene"
  (let [{:keys [fbo width height]} horizontalBlur
        sceneTex (:tex render)]

    (gl/bind fbo)
    (gl/bind sceneTex 1)
    (gl/set-viewport gl 0 0 width height)
    (gl/clear-color-and-depth-buffer globals/gl col/WHITE 1)
    
    (gl/draw-with-shader
     gl
     (-> hBlurQuad
         (assoc-in [:uniforms :sample_offset] (vec2 (/ 1.0 width) 0.0))
         (assoc-in [:uniforms :attenuation] 0.0323)
         ))


    (gl/unbind sceneTex)
    (gl/unbind fbo)

    ))


(defn vertical-blur [gl]
  "Runs the  blur on the scene"
  (let [{:keys [fbo width height]} verticalBlur
        sceneTex (:tex horizontalBlur)]

    (gl/bind fbo)
    (gl/bind sceneTex 1)
    (gl/set-viewport gl 0 0 width height)
    (gl/clear-color-and-depth-buffer globals/gl col/BLACK 1)
    
    (gl/draw-with-shader
     gl
     (-> vBlurQuad
         (assoc-in [:uniforms :sample_offset] (vec2  0.0 (/ 1.0 height)))
         (assoc-in [:uniforms :attenuation] 0.05)))


    (gl/unbind sceneTex)
    (gl/unbind fbo)

    ))




(defn draw-scene [gl]
  (horizontal-blur gl)
  (vertical-blur gl)
  (gl/set-viewport gl 0 0 js/window.innerWidth js/window.innerHeight)

  (gl/bind (:tex verticalBlur) 1)
  (gl/draw-with-shader
   gl
   (-> renderQuad
       (globals/applyGlobalMatrix)))

  (gl/unbind (:tex verticalBlur))
  )


