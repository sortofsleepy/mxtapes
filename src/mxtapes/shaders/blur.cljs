(ns mxtapes.shaders.blur
  (:require-macros
   [macros.helpers :refer [loadfile]])
  (:require
   [mxtapes.globals :as globals]
   [thi.ng.geom.matrix :refer [M44]]
   [thi.ng.geom.vector :refer [vec2]]))

;; builds the horizontal and vertical blur passes to be used in
;; a Composer object
(def blur-shader
   {:vs (loadfile "src/shadersource/blur.vert")
    :fs (loadfile "src/shadersource/blur.frag")
    :uniforms {:view :mat4
               :proj :mat4
               :model [:mat4 M44]
               :tex0 [:sampler2D 1]
               :sample_offset :vec2
               :time :float
               :attenuation [:float 2.5]}
         :attribs {:position :vec2
                   :uv :vec2}
         :varying {:vTexCoord0 :vec2}})

