(ns mxtapes.movements.circular
  (:require-macros
   [macros.helpers :refer [loadfile]])
  (:require
   [thi.ng.geom.gl.core :as gl]
   [thi.ng.geom.gl.shaders :as sh]
   [thi.ng.geom.gl.webgl.constants :as glc]
   [thi.ng.typedarrays.core :as arrays]
   [thi.ng.geom.vector :refer [vec3]]
   [mxtapes.globals :as globals]
   [thi.ng.geom.matrix :refer [M44]]
   [matchstick.pingpong.pingpong :as pbuffer]))




(def noise (loadfile "src/shadersource/noise3d.glsl"))
(def vertex (loadfile "src/shadersource/particlesim/particlerender.vert"))
(def fragment (loadfile "src/shadersource/particlesim/particlerender.frag"))
(def vertex-sim (loadfile "src/shadersource/particlesim/passthru.vert"))
(def fragment-sim (loadfile "src/shadersource/particlesim/particlesim.glsl"))


(defn generate-render []
  {:vs vertex
   :fs fragment
   :attribs {:position :vec3}
   :varying {:vPosition :vec3}
   :state {:depth-test true}
   :uniforms {:proj :mat4
              :view :mat4
              :uScreen [:vec2 [js/window.innerWidth js/window.innerHeight]]
              :uState [:sampler2D 1]}})


(defn generate-sim []
  {:vs vertex-sim
   :fs (str noise fragment-sim)
   :attribs {:position :vec3}
   :uniforms {:proj :mat4
              :view :mat4
              :time :float
              :uState [:sampler2D 1]}})

(def sim (pbuffer/make-pingpong-buffer globals/gl (generate-sim)))


;; ============ FUNCTION ============

(defn get-result []
  (.getOutput sim))
(defn update-sim [t]
  "Update the simulation"
  (.updateWithTime sim t))

(defn bind-movement [unit]
  (.bind-result sim unit)
  )

(defn unbind-movement []
  (.unbind-result sim))

;; ============ DEBUG ============

(defn generatePositions [num]
(let [totalNum (* num 3)]
  (repeatedly totalNum (fn []
                         (let [x (- (* (js/Math.random) 2) 1)
                               y (- (* (js/Math.random) 2) 1)
                               z (- (* (js/Math.random) 2) 1)]
                           (vec3 x y z)))
              )))


(defn gen-particles [num shader]
  (let [spec {:attribs      {:position {:data (arrays/float32 (flatten (generatePositions num)))
                                        :size 3}}
              :mode         glc/points
              :num-vertices num
              :shader       (sh/make-shader-from-spec globals/gl shader)
              }]
    (-> spec
        (gl/make-buffers-in-spec globals/gl glc/static-draw))))




(def particles (gen-particles 4000 (generate-render)))
(defn draw [gl t]

  (.bind-result sim 1)
  (gl/draw-with-shader
    gl
    (-> particles
        (globals/applyGlobalMatrix)))

  (.unbind-result sim)

  )
