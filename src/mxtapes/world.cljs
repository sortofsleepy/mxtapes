(ns mxtapes.world
  (:require
    [mxtapes.vfx :as vfx]
    [mxtapes.globals :as globals]
    [mxtapes.movements.circular :as circ]
    [mxtapes.objects.shard :as shard]))
(def gl globals/gl)

;; draw the scene
(defn draw-world [t]

  (vfx/start-scene gl)
  (shard/draw t (circ/get-result))
  (circ/draw gl t)
  (vfx/end-scene)

  (vfx/start-fx)

  (vfx/draw-scene gl)
  ;; update circurlar movement
  (circ/update-sim t)
  )





