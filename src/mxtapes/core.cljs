(ns mxtapes.core
  (:require
   [mxtapes.world :as world]
   [mxtapes.objects.shard :as shard]
   [mxtapes.shaders.blur]
   [mxtapes.vfx :as vfx]
   [thi.ng.color.core :as col]
   [thi.ng.geom.gl.core :as gl]
   [mxtapes.globals :as globals]
   [thi.ng.geom.gl.webgl.animator :as anim]
   ))

(enable-console-print!)


(anim/animate
 (fn [t frame]
   (gl/set-viewport globals/gl globals/view-rect)
   (gl/clear-color-and-depth-buffer globals/gl col/BLACK 1)
   (world/draw-world t)
  ;; (shard/draw-plane t)

   true))



(defn on-js-reload [])
