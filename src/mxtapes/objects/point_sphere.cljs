(ns mxtapes.objects.point-sphere
  (:require
    [thi.ng.geom.matrix :refer [M44]]
    [matchstick.shaders.utils :as sutils]
    [thi.ng.geom.mesh.subdivision :as sd]
    [thi.ng.geom.polyhedra :as poly]
    [thi.ng.geom.core :as g]
    [thi.ng.typedarrays.core :as arrays]
    [thi.ng.geom.gl.webgl.constants :as glc]
    [thi.ng.geom.gl.shaders :as sh]
    [thi.ng.geom.gl.core :as gl]
    [mxtapes.globals :as globals]))
(enable-console-print!)

(def vertex
  "void main(){
  vec2 vUv = uv;
  vec3 norm = normal;
    gl_PointSize = 2.0;
    gl_Position = proj * view * model * vec4(position,1.);
   }")

(def fragment
  "void main(){
    gl_FragColor = vec4(1.0,0.0,0.0,0.2);
  }")

(def shader-spec (-> (sutils/default-spec vertex fragment)
                     (assoc-in [:state :blend] true)
                     (assoc-in [:state :blend-eq] glc/func-add)
                     ))

(def poly (poly/polyhedron-mesh poly/icosahedron {:iter 1
                                                  :scale 1
                                                  :subdiv sd/catmull-clark}))



(def verts  (->> (g/faces poly)
                 (reduce (fn [acc face-group]
                           (let [accum (atom [])]
                             (doseq [face (g/vertices face-group globals/gl)]
                               (let [[x y z] face]
                                 (reset! accum (conj @accum x y z)))
                               )
                             (conj acc @accum)
                             )
                           ) [])
                 (flatten)))


(def model-spec {:attribs {:position {:data (arrays/float32 verts)
                                      :size 3}}
                 :mode glc/triangle-strip
                 :num-vertices (/ (count verts) 3)
                 :shader (sh/make-shader-from-spec globals/gl shader-spec)})

(def model (-> model-spec (gl/make-buffers-in-spec globals/gl glc/static-draw)))

(defn draw [gl t]
  (gl/draw-with-shader
    gl
    (-> model
        (assoc-in [:uniforms :model] (-> M44
                                         (g/scale 100)
                                         (g/rotate-y (* -1 t))
                                         (g/rotate-x t)
                                         ))
        (globals/applyGlobalMatrix)))
  )
