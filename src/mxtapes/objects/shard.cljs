(ns mxtapes.objects.shard
  (:require-macros
   [macros.helpers :refer [loadfile]])
  (:require
    [thi.ng.geom.matrix :refer [M44]]
    [thi.ng.geom.vector :refer [vec2 vec3]]
    [mxtapes.movements.circular :as circ]
    [matchstick.core.instanceddrawing :as igl]
    [mxtapes.globals :as globals]
    [thi.ng.geom.gl.webgl.constants :as glc]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.geom.gl.shaders :as sh]
    [thi.ng.typedarrays.core :as arrays]
    [thi.ng.math.core :as math]
    [matchstick.core.camera :as mcam]
    [thi.ng.geom.matrix :as mat]
    [thi.ng.geom.gl.buffers :as buf]
    [thi.ng.geom.core :as g]))

;;(def vertex (loadfile "src/shadersource/particlesim/particlerender.vert"))
;;(def fragment (loadfile "src/shadersource/particlesim/particlerender.frag"))
(def gl globals/gl)

(def vertex
  "void main (){
    vColor = color;
    vec3 offsetPos = offset;

    vPosition = position;


      vec4 p = vec4(offset + position,1.);
      vec3 e = normalize(vec3(view * model * p));
      vec3 n = normalize(normalMatrix * normal);
      vec3 r = reflect(e,n);
      float m = 2. * sqrt(pow(r.x,2.) + pow(r.y,2.) + pow(r.z + 1.,2.));
      vNormal.xy =  r.xy / m + .5;
     gl_PointSize = 1.0;

     vec4 simPosition = vec4(texture2D(uState, position.xy + offset.xy).xy / uScreen, 1.0, 1.0);
    gl_Position =  simPosition;
     //gl_Position = proj * view * model * vec4(position + offset,1.0);

  }")


(def fragment
"
void main(){
 vec4 base = texture2D(semtex,vNormal.xy);
 vec2 uv = gl_FragCoord.xy / vec2(1024.0,768.0);
 vec4 color = vec4( vColor );
 color.r += sin( uv.x * 10.0 + time);
 gl_FragColor = mix(color,base,0.5);
}
  ")

(defn generate-render []
  {:vs vertex
   :fs fragment
   :attribs {:position :vec3
             :color :vec4
             :normal :vec3
             :offset :vec3
             }
   :state {:depth-test true}
   :varying {:vPosition :vec3
             :vColor :vec4
             :vNormal :vec3}
   :uniforms {:proj :mat4
              :view :mat4
              :time :float
              :model [:mat4 M44]
              :normalMatrix :mat3
              :semtex [:sampler2D 1]
              :uScreen [:vec2 [js/window.innerWidth js/window.innerHeight]]
              :uState [:sampler2D 0]}})


(def tex-ready (volatile! false))
(def tex (buf/load-texture globals/gl
                           {:callback (fn[tex img] (vreset! tex-ready true))
                            :src "/img/matcap.jpg"
                            :flip true}))
(gl/unbind tex)


(defn build-vertices[]
  (let [num-set (range 0 12)]
    (map-indexed (fn [idx itm]
                   (- (js/Math.random) 0.5)
                   ) num-set) ))


(defn generate-positions [num]
  "Generates offset positions for each triangle for drawing instanced versions"
  (let [totalNum (* num 3)]
    (repeatedly totalNum (fn []
                           (let [x (- (* (js/Math.random) 6) 1)
                                 y (- (* (js/Math.random) 5) 1)
                                 z (- (* (js/Math.random) 6) 1)]
                             (vec3 x y z)))
                )))


(defn generate-orientations [num]
  "Generates offset positions for each triangle for drawing instanced versions"
  (let [totalNum (* num 4)]
    (repeatedly totalNum (fn []
                           (let  [x (- (* (js/Math.random) 2) 1)
                                  y (- (* (js/Math.random) 2) 1)
                                  z (- (* (js/Math.random) 2) 1)
                                  w (- (* (js/Math.random) 2) 1)]
                             [x y z w]))
                )))


(defn build-colors[]
  (let [num-set (range 0 12)]
    (map-indexed (fn [idx itm]
                   (/ (math/random 255) 255)
                   ) num-set) ))



(defn build-field
  ([gl]
    ;; build normals
   (let [vertices (build-vertices)
         colors (build-colors)
         offsets (generate-positions 5000)
         rotation (generate-orientations 5000)
         pA (vec3 (get vertices 0) (get vertices 1) (get vertices 2))
         pB (vec3 (get vertices 3) (get vertices 4) (get vertices 5))
         pC (vec3 (get vertices 4) (get vertices 5) (get vertices 6))
         cb (math/- pC pB)
         ab (math/- pA pB)
         cb (math/cross cb ab)
         cb (math/normalize cb)
         normals (repeatedly 9 (fn [] cb  ))]

     (let [spec {:attribs {:position {:data (arrays/float32 vertices)
                                      :size 3}
                           :color {:data (arrays/float32 colors)
                                   :size 4}

                           :normal {:data (arrays/float32 normals)
                                   :size 2}
                           :offset {:data (arrays/float32 (flatten offsets))
                                    :size 3
                                    :instanced 1}

                           }
                 :mode glc/triangles
                 :num-instances 4000
                 :num-vertices 3
                 :shader (sh/make-shader-from-spec gl (generate-render))}]
       (-> spec (gl/make-buffers-in-spec gl glc/static-draw)))

     )))


(def viewMatrix (mcam/get-view globals/camera))
(def modelMatrix (-> M44))
(def modelViewMatrix (math/* viewMatrix modelMatrix))
(def normalMatrix (mat/matrix44->matrix33 (math/invert (math/transpose modelViewMatrix))))
(def field (build-field gl))


(defn draw [t simupdate]
"Draws the shards. Pass in a delta time and the result texture
from the ping pong simulation."
(when @tex-ready
  (gl/bind tex 1)
  (gl/bind simupdate 0)
  ;;(gl/enable globals/gl glc/blend)
  ;;(gl/disable globals/gl glc/depth-test)

  (igl/instanced-draw
    gl
    (-> field
        (assoc-in [:uniform :model] (-> M44 (g/translate (vec3 -200 -200))))
        (assoc-in [:uniforms :normalMatrix] normalMatrix)
        (assoc-in [:uniforms :time] t)
        (globals/applyGlobalMatrix)))

  (gl/unbind simupdate)
  (gl/unbind tex)
  )

  )