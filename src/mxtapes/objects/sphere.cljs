(ns mxtapes.objects.sphere
  (:require-macros
    [macros.helpers :refer [loadfile]])
  (:require
    [thi.ng.math.core :as math]
    [thi.ng.geom.matrix :as mat :refer [M44]]
    [matchstick.geometry.polyhedra :as sgeometry]
    [mxtapes.globals :as globals]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.geom.gl.buffers :as buf]
    [thi.ng.geom.core :as g]
    [matchstick.shaders.utils :refer [default-spec]]
    [thi.ng.geom.matrix :refer [M44]]
    [matchstick.core.camera :as mcam]))
(enable-console-print!)


;; ========= INITIALIZE SHADER =============
(def vertex (loadfile "src/shadersource/orb.vert"))
(def fragment (loadfile "src/shadersource/orb.frag"))
(def spec (-> (default-spec vertex fragment)
              (update-in [:uniforms] merge {:time :float
                                            :normalMatrix :mat3
                                            :texture :sampler2D})
              (update :varying merge {:vNormal :vec2})))


;; =========== SETUP TEXTURE ===============
(def tex-ready (volatile! false))
(def tex (buf/load-texture globals/gl
                           {:callback (fn[tex img] (vreset! tex-ready true))
                            :src "/img/matcap.jpg"
                            :flip true}))
(gl/unbind tex)

;; ============= SETUP GEOMETRY ==============
(def geo (sgeometry/make-explode-iso globals/gl 1 spec))
(def v2 (mcam/get-view globals/camera))

(defn draw [t]

  (when @tex-ready
    (let [viewMatrix v2
          modelMatrix (:model (:uniforms geo))
          modelViewMatrix (math/* viewMatrix modelMatrix)
          normalMatrix (mat/matrix44->matrix33 (math/invert (math/transpose modelViewMatrix)))
          geoUpdate (-> geo
                        (globals/applyGlobalMatrix)
                        (update :uniforms merge {:normalMatrix normalMatrix
                                                 :time t
                                                 :model (-> M44
                                                            (g/scale 40)
                                                            (g/rotate-y t)
                                                            (g/rotate-x t))}))]


      (gl/bind tex)
      (gl/draw-with-shader
        globals/gl
        geoUpdate)
      (gl/unbind tex)



      ;; because the correct normal matrix is depended on the current state of the
      ;; model view matrix, we need to overwrite preveious value of geo so things get updated.
      (set! geo geoUpdate))

    )
  )

