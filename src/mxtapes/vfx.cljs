(ns mxtapes.vfx
  (:require-macros
    [macros.helpers :refer [loadfile]])
  (:require


    [thi.ng.geom.gl.webgl.constants :as glc]
    [thi.ng.color.core :as col]
    [thi.ng.geom.vector :refer [vec2]]
    [mxtapes.globals :as globals]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.geom.gl.shaders :as sh]
    [thi.ng.geom.matrix :refer [M44]]
    [mxtapes.globals :as globals]
    [thi.ng.geom.gl.fx :as fx]
    [matchstick.core.fbo :as fbo]))

;; runs a horizontal and vertical blur pass on a scene which can be written to
;; using the begin/end scene functions

(def gl globals/gl)


(def passthrough-vs
  "void main(){
  vUV=uv;
  gl_Position=  model * vec4(position,0.0,1.0);}")

(def passthrough-fs
  "void main(){
  gl_FragColor=texture2D(testtex,vUV);

  }")

(def shader-spec
  {:vs passthrough-vs
   :fs passthrough-fs
   :attribs  {:position  :vec2
              :uv        :vec2}
   :varying  {:vUV       :vec2}
   :uniforms {:model     [:mat4 M44]
              :proj      :mat4
              :view      :mat4
              :testtex [:sampler2D 2]
              :tex       [:sampler2D 0]}
   :state    {:depth-test true
              :blend-eq glc/func-add}})

(def blur-shader
  {:vs (loadfile "src/shadersource/blur.vert")
   :fs (loadfile "src/shadersource/blur.frag")
   :uniforms {:view :mat4
              :proj :mat4
              :model [:mat4 M44]
              :tex0 [:sampler2D 1]
              :sample_offset :vec2
              :attenuation :float}
   :attribs {:position :vec2
             :uv :vec2}
   :varying {:vTexCoord0 :vec2}})


(defn initialize-pipeline [gl passes render]
  (let [idx (atom -1)]
    (reduce (fn [acc {:keys [framebuffer uniforms quad meta] :as v}]
              (let [shader (:shader quad)
                    width (:width framebuffer)
                    height (:height framebuffer)
                    pass (update shader :uniforms merge uniforms)
                    pass (assoc quad :shader pass :fbo framebuffer :width width :height height)]

                (reset! idx (inc @idx))
                (if (< (dec @idx) 0)
                  (do
                    (conj acc (assoc pass :input (:tex (:framebuffer render)))))
                  (let [prevPass (get passes (dec @idx))]
                    (conj acc (assoc pass :input (:tex (:framebuffer prevPass))))
                    )
                  )
                )
              )
            [] passes)
    )
  )


(defn build-pass [gl & {:keys [width height shader] :or {width 512
                                                         height 512
                                                         shader nil}}]

  (let [fbo (fx/init-fx-fbo globals/gl 512 512)
        quad (-> (fx/init-fx-quad globals/gl)
                 (assoc :shader (sh/make-shader-from-spec globals/gl shader))
                 (assoc-in [:shader :state :tex] (:tex fbo)))]

    {:fbo fbo
     :framebuffer fbo
     :quad quad}

    )


  )
(def verticalBlur (build-pass globals/gl :shader blur-shader))
(def horizontalBlur (build-pass globals/gl :shader blur-shader))
(def render (build-pass globals/gl :shader shader-spec))

(def pipes (initialize-pipeline globals/gl [horizontalBlur verticalBlur] render))

;; Binds the scene fbo so that we can render the scene into it
(defn start-scene [gl]
  (let [{:keys [fbo width height]} (:fbo render)]
    (gl/bind fbo)    ;; set viewport
    (gl/set-viewport gl 0 0 width height)
    (gl/clear-color-and-depth-buffer globals/gl col/BLACK 1)

    ))


;; ends the rendering onto the scene fbo
(defn end-scene []
  (let [{:keys [fbo]} (:fbo render)]
    (gl/unbind fbo)))



(defn run-pipeline []
  (loop [pass pipes]
    (when pass
      (let [spec (first pass)
            {:keys [fbo width height]} (:fbo spec)
            sceneTex (:input spec)]


        (gl/bind fbo)
        (gl/bind sceneTex 1)
        (gl/set-viewport gl 0 0 width height)
        (gl/clear-color-and-depth-buffer globals/gl col/BLACK 1)

        (gl/draw-with-shader
          gl
          (-> spec
              (assoc-in [:uniforms :sample_offset] (vec2  0.0 (/ 1.0 height)))
              (assoc-in [:uniforms :attenuation] 0.020)))


        (gl/unbind sceneTex)
        (gl/unbind fbo)

        )

      (recur (next pass))))
  )

(defn start-fx []
  (run-pipeline))


(defn draw-scene [gl]
  (let [output (last pipes)
        tex (:tex (:fbo output))]
    (gl/set-viewport gl 0 0 js/window.innerWidth js/window.innerHeight)

    (gl/bind tex 2)
    (gl/enable gl glc/blend)
    (gl/draw-with-shader
      gl
      (-> (:quad render)
          (globals/applyGlobalMatrix)))

    (gl/unbind tex)
    ))
