(ns mxtapes.globals
  (:require
    [matchstick.core.camera :as mcam]
    [thi.ng.geom.gl.core :as gl]
    [thi.ng.geom.rect :as r]
    [thi.ng.geom.vector :as v]
    [thi.ng.geom.gl.camera :as cam]))
(enable-console-print!)



(def gl  (gl/gl-context "main"))

(def view-rect (gl/get-viewport-rect gl))
(def canvas (.querySelector js/document "#main"))
(set! view-rect (r/rect 0 0 js/window.innerWidth js/window.innerHeight))

(def camera (mcam/make-camera {:aspect view-rect
                               :near   1.0
                               :far    10000.0
                               :fov    60}))

(defn updateCameraMatrices []
  "resize the camera and canvas to make sure everything looks right"
  (aset canvas "width" js/window.innerWidth)
  (aset canvas "height" js/window.innerHeight)

  ;; set view rect
  (set! view-rect (r/rect 0 0 js/window.innerWidth js/window.innerHeight))

  (mcam/updateProjection camera js/window.innerWidth js/window.innerHeight)
  (mcam/setEye camera (v/vec3 0 0 50))
  ;;(mcam/enableArcball camera)

  )



(defn applyGlobalMatrix [spec]
  (mcam/applyTo camera spec))



;; add listener to ensure camera adjusts
(.addEventListener js/window "resize" updateCameraMatrices)

(updateCameraMatrices)
